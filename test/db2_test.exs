defmodule Db2Test do
  use ExUnit.Case
  doctest Db2
  ecto = Mix.Project.deps_paths[:ecto]
  Code.require_file "#{ecto}/integration_test/cases/assoc.exs", __DIR__
  Code.require_file "#{ecto}/integration_test/cases/interval.exs", __DIR__
end
